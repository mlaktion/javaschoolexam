package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException{
        if (inputNumbers.contains(null))
            throw new CannotBuildPyramidException();
        int numCnt = inputNumbers.size();
        int i = 1;
        while (numCnt > 0) {
            numCnt = numCnt - i;
            i++;
        }

        if (numCnt < 0)
            throw new CannotBuildPyramidException();
        i--; //number of lines & numbers in the last row

        Collections.sort(inputNumbers);

        int [][] pyramid = new int[i][i +  i - 1];
        int arrHeight = i;

        int j = 0;
        int minIndex = i-1;
        int maxIndex = i-1;

        int currNumIndex = 0;

        while (j < arrHeight){
            for(int k = minIndex; k <= maxIndex; k = k + 2){
                pyramid[j][k] = inputNumbers.get(currNumIndex);
                currNumIndex++;
            }
            minIndex -= 1;
            maxIndex += 1;
            j++;
        }
        return pyramid;
    }
}
