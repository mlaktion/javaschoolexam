package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null)
            return null;
        Pattern p = Pattern.compile("[\\+\\-*\\/.]{2,}");
        Matcher m = p.matcher(statement);

        if (m.find() || !statement.matches("[0-9\\+\\-/*.()]+"))
            return null;

        int cntParentheses = 0;
        LinkedList<Double> nums = new LinkedList<>();
        LinkedList<Character> op = new LinkedList<>();
        try {
            for (int i = 0; i < statement.length(); i++) {
                char c = statement.charAt(i);
                switch (c) {
                    case '(':
                        op.add('(');
                        cntParentheses++;
                        break;
                    case ')':
                        if (op.isEmpty() || !op.contains('('))  //case 2*4)+(3 -  invalid
                            return null;
                        while (op.getLast() != '(')
                            processOperator(nums, op.removeLast());
                        op.removeLast();
                        cntParentheses--;
                        break;
                    case '+':
                    case '-':
                    case '*':
                    case '/':
                        while (!op.isEmpty() && priority(op.getLast()) >= priority(c))
                            processOperator(nums, op.removeLast());
                        op.add(c);
                        break;
                    default:
                        String operand = "";
                        while (i < statement.length() &&
                                (Character.isDigit(statement.charAt(i)) ||
                                        statement.charAt(i) == '.'))
                            operand += statement.charAt(i++);
                        i--;
                        nums.add(Double.parseDouble(operand));
                }
            }
            if (cntParentheses != 0)
                return null;
            while (!op.isEmpty())
                processOperator(nums, op.removeLast());

            Double res = nums.get(0);
            if (res % 1 == 0)
                return res.intValue() + "";
            else
                return res.toString();
        } catch (ArithmeticException e) {
            return null;
        }
    }
    private static int priority(char op) {
        switch (op) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;
        }
    }
    private static void processOperator(LinkedList<Double> st, char op) {
        double r = st.removeLast();
        double l = st.removeLast();
        switch (op) {
            case '+':
                st.add(l + r);
                break;
            case '-':
                st.add(l - r);
                break;
            case '*':
                st.add(l * r);
                break;
            case '/':
                if (r == 0)
                    throw new ArithmeticException();
                st.add(l / r);
                break;
        }
    }
}
