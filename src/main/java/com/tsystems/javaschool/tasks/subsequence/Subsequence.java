package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException{
        if (x == null || y == null)
            throw new IllegalArgumentException();

        if (x.size() > y.size())
            return false;

        int currXIndex = 0;
        int i = 0;
        int xSize = x.size();
        int ySize = y.size();
        while (currXIndex < xSize) {
            Object currXItem = x.get(currXIndex);

            while(i < ySize) {
                Object currYItem = y.get(i);
                i++;
                if (currXItem.equals(currYItem)){
                    currXIndex++;
                    break;
                }
            }
            if (i == ySize && currXIndex != xSize)
                return false;
        }

        return true;
    }
}
